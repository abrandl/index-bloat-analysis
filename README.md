### Index bloat analysis

1. `pg96-original`: Original PG 9.6 database (GitLab.com, April 2020)
1. `pg*-reindexed`: Recreating indexes with `REINDEX`
1. `pg*-incremental-load`: Load schema first, including indexes and then data using `COPY`

Step (3) is the actually relevant one for the analysis.

