#!/bin/bash
datadir="data-$(date +%F-%H%M%S)"
psql="sudo gitlab-psql"

if [[ -z "${datadir}" ]]; then
  echo "parameter missing: datadir"
  exit -1
fi

if [[ -d "${datadir}" ]]; then
  echo "datadir exists: ${datadir}"
  exit -2
fi

mkdir -p $datadir

cat meta.sql | $psql -t > $datadir/meta
cat config.sql | $psql > $datadir/config
cat index-sizes.sql | $psql > $datadir/index-sizes.csv
cat rel-sizes.sql | $psql > $datadir/rel-sizes.csv
cat pg_class.sql | $psql > $datadir/pg_class.csv
cat bloat_btree.sql | $psql -q > $datadir/bloat_btree.csv
cat bloat_table.sql | $psql -q > $datadir/bloat_table.csv
